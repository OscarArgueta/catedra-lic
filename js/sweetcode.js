$('document').ready(function(){
    initializeData();
});

function initializeData() {
    
    //Verificando si los datos no existen no existen
    if (!localStorage.getItem("nombre")) {
        //Si no existen, deben ser inicializados
        localStorage.setItem("nombre", "Ash Ketchum");
        localStorage.setItem("pin", "1234");
        localStorage.setItem("cuenta", "0987654321");
        localStorage.setItem("saldo", "500");
        localStorage.setItem("depositoCount", "0");
        localStorage.setItem("retiroCount", "0");
        localStorage.setItem("consultaCount", "0");
        localStorage.setItem("pagoCount", "0");
    }
    
}

function depositar() {
  swal({
    content: {
      element: "input",
      attributes: {
        placeholder: "Ingrese la cantidad que desea depositar",
        type: "text",
      },
    },
  }).then((value) => {
    //agregar codigo de operaciones aca acceder al valor retornado con ${value}
      
        if (value == ""){//Input vacio
          swal("Error", "Por favor, ingresa una cantidad", "error");
        }
      
        else if ((value * 100) % 1 != 0 && !isNaN((value * 100) % 1)){//Valores decimales mayores a 2
          swal("Error", "Por favor, ingresa valores de hasta dos decimales", "error");
        }
      
        else if (value <= 0){//Valores no positivos
          swal("Error", "Por favor, ingresa valores positivos", "error");
        }
      
        else if (value > 0) {
          //Actualizando saldo
          var saldoOriginal = parseFloat(localStorage.getItem("saldo"));
          var saldo = saldoOriginal + parseFloat(`${value}`);
          localStorage.setItem("saldo", saldo);

          //Actualizando auditoria
          var depositoCount = parseInt(localStorage.getItem("depositoCount")) + 1;
          localStorage.setItem("depositoCount", depositoCount);

          //operación exitosa, opcion de imprimir reporte
          imprimir("Depósito realizado con éxito", "depositar", saldoOriginal, parseFloat(`${value}`));
        }

        else {//Cualquier otro error
          swal("Error", "Por favor, ingresa valores válidos", "error");
        }
    
  });
}

function retirar() {
  swal({
    content: {
      element: "input",
      attributes: {
        placeholder: "Ingrese la cantidad que desea retirar",
        type: "text",
      },
    },
  }).then((value) => {
    //agregar codigo de operaciones aca acceder al valor retornado con `${value}`

        //Saldo actual
        var saldo = parseFloat(localStorage.getItem("saldo"))
      
        if (value == ""){//Input vacio
          swal("Error", "Por favor, ingresa una cantidad", "error");
        }
      
        else if ((value * 100) % 1 != 0 && !isNaN((value * 100) % 1)){//Valores decimales mayores a 2 caracteres
          swal("Error", "Por favor, ingresa valores de hasta dos decimales", "error");
        }
      
        else if (value <= 0){//Valores no positivos
          swal("Error", "Por favor, ingresa valores positivos", "error");
        }
      
        else if (value > saldo) {//Valores mayores al saldo actual
          swal("Error", "La cantidad ingresada excede los fondos de tu cuenta", "error");
        }
      
        else if (value > 0) {
          var saldoOriginal = saldo
          //Actualizando saldo
          saldo -= parseFloat(`${value}`);
          localStorage.setItem("saldo", saldo);

          //Actualizando auditoria
          var retiroCount = parseInt(localStorage.getItem("retiroCount")) + 1;
          localStorage.setItem("retiroCount", retiroCount);

          //operación exitosa, opcion de imprimir reporte
          imprimir("Retiro realizado con éxito", "retirar", saldoOriginal, parseFloat(`${value}`));
        }

        else {//Cualquier otro error
          swal("Error", "Por favor, ingresa valores válidos", "error");
        }
  });
}

function consultar() {
  //obtener los datos y mostrarlos
  swal("Saldo actual", "Su saldo disponible es: $" + parseFloat(localStorage.getItem("saldo")).toFixed(2));
    
  //Actualizando auditoria
  var consultaCount = parseInt(localStorage.getItem("consultaCount")) + 1;
  localStorage.setItem("consultaCount", consultaCount);
}

//TODO: implementar esta parte
function pagar(pago) {
  swal({
    content: {
      element: "input",
      attributes: {
        placeholder: "Ingrese el monto a cancelar",
        type: "text",
      },
    },
  }).then((value) => {
    //agregar codigo de operaciones aca acceder al valor retornado con ${value}

    //Saldo actual
        var saldo = parseFloat(localStorage.getItem("saldo"))
      
        if (value == ""){//Input vacio
          swal("Error", "Por favor, ingresa una cantidad", "error");
        }
      
        else if ((value * 100) % 1 != 0 && !isNaN((value * 100) % 1)){//Valores decimales mayores a 2 caracteres
          swal("Error", "Por favor, ingresa valores de hasta dos decimales", "error");
        }
      
        else if (value <= 0){//Valores no positivos
          swal("Error", "Por favor, ingresa valores positivos", "error");
        }
      
        else if (value > saldo) {//Valores mayores al saldo actual
          swal("Error", "La cantidad ingresada excede los fondos de tu cuenta", "error");
        }
      
        else if (value > 0) {
          //Actualizando saldo
          var saldoOriginal = saldo;
          saldo -= parseFloat(`${value}`);
          localStorage.setItem("saldo", saldo);

          //Actualizando auditoria
          var pagoCount = parseInt(localStorage.getItem("pagoCount")) + 1;
          localStorage.setItem("pagoCount", pagoCount);

          //operación exitosa, opcion de imprimir reporte
          imprimir("Factura pagada", "pago", saldoOriginal, `${value}`, pago);
        }

        else {//Cualquier otro error
          swal("Error", "Por favor, ingresa valores válidos", "error");
        }
  });
}


function imprimir(titulo, accion, val1, val2, val3) {
  swal({
      title: titulo,
      icon: "success",
      text: "¿Desea imprimir un comprobante?", 
      buttons: {
        cancel: "No, gracias",
        catch: {
          text: "Si",
          value: "imprimir",
        },
      },
    })
    .then((value) => {
      switch (value) {

        case "imprimir":
          // Obtenemos la fecha actua
          var hoy = new Date();

          var date = hoy.getDate() + "/" + (hoy.getMonth()+1) + "/" + hoy.getFullYear()
          var time = hoy.getHours()+":"+hoy.getMinutes();

          var ID = parseInt(localStorage.getItem("depositoCount"));
          ID += parseInt(localStorage.getItem("retiroCount"));
          ID += parseInt(localStorage.getItem("consultaCount"));
          ID += parseInt(localStorage.getItem("pagoCount"));

          establecerInformacion(ID, date, time)


          // Obtenemos la informacion
          var nombre = localStorage.getItem("nombre");
          var cuenta = localStorage.getItem("cuenta");

          // Informacion sobre el saldo
          var saldo = parseFloat(localStorage.getItem("saldo"));
          establecerFilasDelUsuario(nombre, cuenta, saldo)

          // Si pagaremos algun servicio
          switch (accion) {
            case "pago":
            agregarPrecios(parseInt(localStorage.getItem("pagoCount")), "Usted ha decidido pagar el servicio de " + val3, val1, val2)
            generarPDF()
              break;
          
            case "retirar":
            establecerRetiro(parseInt(localStorage.getItem("retiroCount")), val1, val2)
            generarPDF()
              break;
            
            case "depositar":
            establecerDeposito(parseInt(localStorage.getItem("retiroCount")), val1, val2)
            generarPDF()
              break;
          }

          swal("Hecho", "Comprobante impreso", "success");
          break;

        default:
          swal("No se imprimió comprobante");
      }
    });
}