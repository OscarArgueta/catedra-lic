$(document).ready(function () {
    login("Bienvenido a PokeBank");
});
    
function login(titulo) {
    $(".filter").hide();
    $('.modal').modal();
    swal({
        title: titulo,
        closeOnEsc: false,
        closeOnClickOutside: false,
        content: {
            element: "input",
            attributes: {
                placeholder: "Ingrese su pin",
                type: "password",
            },
        },
    }).then((value) => {
        //agregar codigo de operaciones aca acceder al valor retornado con ${value}

        if (`${value}` == localStorage.getItem("pin")) { 
            swal("Hecho", "Bienvenido!", "success");
            $(".filter").show();
            
            //Establecioendo nombre y numero de cuenta del usuario
            $('#linkNombre').html(localStorage.getItem("nombre"));
            $('#linkCuenta').html("Número de cuenta: " + localStorage.getItem("cuenta"));
            return true;
        } else {
            //location.reload();
            login("El PIN ingresado es incorrecto");
        }
    });
}

function logout(){
    location.reload();
}