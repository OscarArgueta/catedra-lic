//Objeto chart
var Chart1 = new Chart();

function makeChart() {
    //Destruyendo grafica anterior
    if (typeof Chart1 !== 'undefined') {
        Chart1.destroy();
    }
    //Si la grafica no se destruyece antes, ocurririan bugs
    
    //objeto canvas
    var cht = document.getElementById("Chart1");
    
    //Obteniendo registros de auditoria
    var deposito = localStorage.getItem("depositoCount");
    var retiro = localStorage.getItem("retiroCount");
    var consulta = localStorage.getItem("consultaCount");
    var pago = localStorage.getItem("pagoCount");
    
    //Generando gráfico de pastel
    Chart1 = new Chart(cht, {
       type: 'pie',
       data: {
           //Nombres de cada elemento
           labels: ["Depósitos", "Retiros", "Consultas de saldo", "Pagos de facturas"],
           datasets:[{
               //Datos
               data: [deposito, retiro, consulta, pago],
               
               //Color de cada elemento
               backgroundColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(153, 102, 255, 1)'
               ],
               
               //Bordes blancos
               borderColor: [
                    'rgb(255, 255, 255)',
                    'rgb(255, 255, 255)',
                    'rgb(255, 255, 255)',
                    'rgb(255, 255, 255)'
               ],
               
               borderWidth: 2
           }]
       }
    });
}